# OpenVPN Client Add-On for Home Assistant

## Installation

1. Move your certification file (client.ovpn) to hassio/config folder.
2. Add the repository URL to HA Add-On : https://bitbucket.org/clara-team/hub-vpn-client
3. Search in the Add-Ons section for Clara Cloud Client Connection, then INSTALL and Start the add-on